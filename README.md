<h1>Pig-Game-DICE</h1>

Pig Game is a simple Dice game with the following rules :-

    The game has 2 players, playing in rounds
    In each turn, a player rolls a dice as many times as he/she wants. Each result gets added to the ROUND score
    BUT, if the player rolls a 1, all the ROUND score gets lost. After that, it's the next player's turn
    The player can choose to 'Hold', which means that his ROUND score gets added to his GLOBAL score. After that, it's the next player's turn
    The first player to reach 100 points on GLOBAL score wins the game

The game is all about luck.

This is a web based version of the game and is currently compactible only for desktop browsers.

